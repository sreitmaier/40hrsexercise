import React from "react"
import Image21 from "../components/image21"
import Image22 from "../components/image22"
import Layout from "../components/layout"
import SEO from "../components/seo"

const NimwayPage = () => (
  <Layout>
    <SEO title="Nimway Care" />
    <div style={{ maxWidth: `700px`, marginBottom: `1.45rem` }}>
      <h2>Nimway Care</h2>
      <h5>IoT UseCase | B2C</h5>
    </div>
    <div className="image-container" 
    >
      <Image21 />
    </div>

    <div
      style={{ maxWidth: `700px`, lineHeight: 1.3, marginBottom: `1.45rem` }}
    >
      <h3>Problem</h3>
      <p>
       Orientation is not there when you need it most - in the hospital. Use
        Case Development for an indoor navigation 
        technology in cooperation with TU Darmstadt and Sony, Sweden.
      </p>
    </div>

    <div
      style={{ maxWidth: `700px`, lineHeight: 1.3, marginBottom: `1.45rem` }}
    >
      <h3>Solution</h3>
      <p>
        A patient centric ioT system offering wayfinding and an individual knowledge base.  
      </p>
    </div>


    <div
      style={{ maxWidth: `700px`, lineHeight: 1.3, marginBottom: `1.45rem` }}
    >
      <h3>Research</h3>
      <p>
        As part of the generative research an empathy map for Nimway Care was created.
      </p>
    </div>

    <div
      style={{
        maxWidth: `400px`,
        marginBottom: `2rem`,
       
      }}
    >
      <Image22 />
    </div>


  </Layout>
)

export default NimwayPage
