import React from "react"
import { Link } from "gatsby"
import Image21 from "../components/image21"
import Image2 from "../components/image2"
import Layout from "../components/layout"

import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />

    <div style={{ maxWidth: `700px`, marginBottom: `3.45rem` }}>
      <h2>Hi, How Are You? </h2>
      <p>
        I´m Sabine. Currently i'm working as design consultant at Boldly Go Industries, an
        innovation and technology consulting agency based in Frankfurt am Main.
        What i'm up to? You can check my{" "}
        <a
          style={{ color: `black`, textDecoration: `underline` }}
          href="https://sabinereitmaier.hashbase.io"
          target="_blank"
        >
          Small Blog.
        </a>{" "}
        I'm posting things there every other week or at least once a month :))).
        Want to know how i work? Check the case studies below. Want to see more?
        Just send me an email – i'm looking forward to getting in touch with
        you.
      </p>
    </div>

    <div className="image-container">
      <Image2 />
    </div>

    <div className="navbox-container">
      <div className="text-nav">
        
          <h3>Caddo </h3>
          <p>
            Caddo is your foodie friend, carefully selecting food trucks based
            on your personal settings.
          </p>
          <Link
            style={{ color: `green` }}
            to="/caddo/"
          >
            Case Study
          </Link>
        
      </div>
    </div>

    <div className="image-container">
      <Image21 />
    </div>

    <div className="navbox-container">
      <div className="text-nav">
        
          <h3> nimway</h3>
          <p>
            Wayfinding und knowledge made easy where you need it most: As a patient in the
            hospital.
          </p>

          <div>
          <Link
            style={{ color: `orange` }}
            to="/nimway/"
          >
            Case Study
          </Link>
          </div>
        
      </div>
    </div>
  </Layout>
)

export default IndexPage
