# Coding Exercise  :stuck_out_tongue:  :smiley_cat:
## Build and deploy a react-based site with standard gatsby starter.

Should match existing sites: sabinereitmaier.com and sabinereitmaier.hashbase.io

Should keep dat blog accessible and have a no javascript option 

See a first version  at https://happy-hypatia-20a081.netlify.com/

(developed mobile first for devices 375 x 667)


![Alternate text](blog.png)


![Alternate text](site.png)



