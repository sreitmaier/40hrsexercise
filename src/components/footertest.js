import PropTypes from "prop-types"
import React from "react"

const Footer = ({ titleFooter }) => (
  <footer
    style={{
      background: `white`,
      marginBottom: `1.45rem`,
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
        
      }}
    >
     { titleFooter }
    </div>
  </footer>
)

Footer.propTypes = {
  titleFooter: PropTypes.string,
}

Footer.defaultProps = {
  titleFooter: ``,
}

export default Footer
