/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"

import Header from "./header"
import "./layout.css"

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <div className="site-container">
          <div className="header-container">
            <Header siteTitle={data.site.siteMetadata.title} />
          </div>

          <div
            style={{
              margin: `0 auto`,
              maxWidth: 900,
              padding: `0px 1.0875rem 1.45rem`,
              paddingTop: 10,
              
             
            }}
          >
            <main>{children}</main>

            <div className="footer-container">
              <footer>© {new Date().getFullYear()}, Sabine Reitmaier</footer>
            </div>
          </div>
          
        </div>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
