import React from "react"
import Image3 from "../components/image3"
import Image2 from "../components/image2"
import Image4 from "../components/image4"
import Image5 from "../components/image5"
import Image6 from "../components/image6"
import Image21 from "../components/image21"
import Layout from "../components/layout"
import { Link } from "gatsby"
import SEO from "../components/seo"

const CaddoPage = () => (
  <Layout>
    <SEO title="Caddo" />
    <div style={{ maxWidth: `700px`, marginBottom: `1.45rem` }}>
      <h2>Caddo</h2>
      <h5>Multi-Platform App | B2C</h5>
    </div>
    <div className="image-container">
      <Image2 />
    </div>
    <div
      style={{ maxWidth: `700px`, lineHeight: 1.3, marginBottom: `1.45rem` }}
    >
      <h3>Problem</h3>
      <p>
        During lunchbreak, food is not available where you would want it. What
        about mobile food services? How to find them?
      </p>
    </div>
    <div
      style={{ maxWidth: `700px`, lineHeight: 1.3, marginBottom: `1.45rem` }}
    >
      <h3>Solution</h3>
      <p>
        A multi-platform conversational app based around the character of Caddo.
      </p>
      <h5>Caddo as Mobile App </h5>
      <p>
        Caddo is centered around a vertical scroll interface with informational
        elements in the form of chat bubbles.
      </p>
      <div className="image-container">
        <Image5 />
      </div>
      <h5>Daily Food Truck Selection </h5>
      <p>
        Based on your selection of favourite foods, Caddo will provide a list of
        food trucks sorted by proximity to your current location.
      </p>
      <div className="image-container">
        <Image4 />
      </div>
      <h5>Taste ‘n’ Listen </h5>
      <p>
        Scientists at the Crossmodal Research Laboratory have noticed that sound
        can affect the experience of certain flavours. Find out if you can
        improve the umami, sweet or sour flavours of your meals by listening to
        the sounds of Taste ‘n’ Listen.
      </p>
    </div>
    <div className="image-container">
      <Image3 />
    </div>
    <h5>Caddo as Website </h5>
    <p>
      Caddo on the web is centered around a vertical scroll interface that
      offers textual information about their services and platforms in a relaxed
      dialogue style.
    </p>
    <div className="image-container">
      <Image6 />
    </div>
    <h5>Account Set-Up/ Management</h5>
    <p>
      In the title bar you will find an immediate way to manage your profile and
      your selection of preferred ingredients.
    </p>
    <h5>Caddo as messenger bot</h5>
    <p>
      Caddo still delivers the personal food truck report, but as a bot they
      really love to talk about food. Besides their favourite recipes, they
      sometimes tell a joke.
    </p>

    <h5>Caddo on Instagram</h5>
    <p>
      On Instagram Caddo gets to travel to various cities. Caddo will meet the
      people behind the food trucks and post about the food they love. Every
      other week Caddo will invite a chef to take over the account and visualize
      what happens behind the scenes of a food truck.
    </p>

    <div
      style={{ maxWidth: `400px`, marginTop: `3rem`, marginBottom: `1.45rem` }}
    >
      <p>
        <Link
          style={{ color: `black`, textDecoration: `underline` }}
          to="/nimway/"
        >
          Next Case Study
        </Link>
      </p>
    </div>

    <div className="image-container">
      <Image21 />
    </div>
  </Layout>
)

export default CaddoPage
