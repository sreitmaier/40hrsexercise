import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const AboutPage = () => (
  <Layout>
    <SEO title="About" />
    <h2>Hi, How Are You? </h2>
    <p>
      Currently i'm working as design consultant at Boldly Go Industries, an innovation and technology
      consulting agency based in Frankfurt am Main. What i'm up to? You can
      check my{" "}
      <a
        style={{ color: `blue`, textDecoration: `none` }}
        href="https://sabinereitmaier.hashbase.io"
        target="_blank"
      >
        Small Blog.
      </a>{" "}
      I'm posting things there every other week or at least once a month :))).
      Check the case studies to get to know how i work or to see more, just send me an email – i'm looking forward to
      getting in touch with you.
    </p>
  </Layout>
)

export default AboutPage
